from core.views import CreateGroupView, \
    CreateStudentView, \
    GroupView, \
    StudentView, \
    UpdateStudentView,\
    SendMailView,\
    RegistrationView, \
    TeacherAccountsView, \
    DownloadStudentList,\
    DownloadGroupList

from django.contrib.auth import views
from django.urls import path


app_name = "core"

urlpatterns = [
    path('group/create/', CreateGroupView.as_view(),
         name='create-group'),
    path('student/create/', CreateStudentView.as_view(),
         name='create-student'),
    path('student/update/<int:pk>', UpdateStudentView.as_view(),
         name='update-student'),
    path('student/list', StudentView.as_view(), name='students-list'),
    # path('student/delete/<int:pk>', DeleteStudentView.as_view(),
    #      name='delete-student'),
    path('registr/', RegistrationView.as_view(), name='registration-user'),
    path('send/mail/', SendMailView.as_view(), name='send-mail'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('account/user/', TeacherAccountsView.as_view(), name='account-user'),
    path('downnload/student/csv/',
         DownloadStudentList.as_view(),
         name='download-students-list'),
    path('downnload/group/csv/',
         DownloadGroupList.as_view(),
         name='download-group-list'),
    path('', GroupView.as_view(), name='home'),
]
