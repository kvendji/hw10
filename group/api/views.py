from rest_framework.viewsets import ModelViewSet

from api.serializers import TeacherSerializer,\
    StudentSerializer,\
    GroupSerializer
from core.models import TeacherUser, Student, Group


class TeacherViewSet(ModelViewSet):
    queryset = TeacherUser.objects.all()
    serializer_class = TeacherSerializer


class StudentViewSet(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
