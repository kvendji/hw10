from django.utils.deprecation import MiddlewareMixin
from core.models import Logger


class LogMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if request.path != '/admin/':
            logger = Logger(
                path=request.path,
                method=request.method,
            )
            logger.save()

    def process_response(self, request, response):
        return response
