from rest_framework import serializers
from core.models import Group, Student, TeacherUser


class ShortTeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeacherUser
        fields = ['first_name', 'last_name']


class GroupSerializer(serializers.ModelSerializer):
    teacher = ShortTeacherSerializer()

    class Meta:
        model = Group
        fields = '__all__'


class StudentSerializer(serializers.ModelSerializer):
    group = GroupSerializer()

    class Meta:
        model = Student
        fields = '__all__'


class TeacherSerializer(serializers.ModelSerializer):

    class Meta:
        model = TeacherUser
        fields = ['first_name', 'last_name', ]
