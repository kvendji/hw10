from django.test import TestCase
from django.urls import reverse_lazy

from core.models import Student, Group, TeacherUser


class DiagnosticTestCase(TestCase):

    def test_account_user_is_access(self):
        response = self.client.get(reverse_lazy('core:account-user'))
        assert response.status_code == 200

    def test_create_group_is_access(self):
        response = self.client.get(reverse_lazy('core:create-group'))
        assert response.status_code == 200

    def test_create_student_is_access(self):
        response = self.client.get(reverse_lazy('core:create-student'))
        assert response.status_code == 200

    def test_update_student_is_access(self):
        teacher = TeacherUser.objects.create()
        group = Group.objects.create(
            teacher=teacher
        )
        student = Student.objects.create(
            age=10,
            group=group
        )
        response = self.client.get(reverse_lazy('core:update-student',
                                                args=[student.id]))
        assert response.status_code == 200

    def test_students_list_is_access(self):
        response = self.client.get(reverse_lazy('core:students-list'))
        assert response.status_code == 200

    def test_registration_user_is_access(self):
        response = self.client.get(reverse_lazy('core:registration-user'))
        assert response.status_code == 200

    def test_home_is_access(self):
        response = self.client.get(reverse_lazy('core:home'))
        assert response.status_code == 200

    def test_send_mail_is_access(self):
        response = self.client.get(reverse_lazy('core:send-mail'))
        assert response.status_code == 200

    def test_count_group(self):
        teacher = TeacherUser.objects.create()
        self.client.force_login(teacher)
        groups_count = 5
        for i in range(groups_count):
            Group.objects.create(teacher=teacher)
        response = self.client.get(reverse_lazy('core:account-user'))
        self.assertEqual(response.context['user'].teacher_groups.count(),
                         groups_count)

    def test_count_students(self):
        teacher = TeacherUser.objects.create()
        self.client.force_login(teacher)
        group = Group.objects.create(teacher=teacher)
        students_count = 10
        for i in range(students_count):
            Student.objects.create(
                group=group,
                age=students_count
            )
        response = self.client.get(reverse_lazy('core:account-user'))
        self.assertEqual(response.context['user'].
                         teacher_groups.students.count(),
                         students_count)
