from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from group import celery_app
from core.models import Logger, TeacherUser
from datetime import timedelta, datetime
from rest_framework.authtoken.models import Token


@celery_app.task
def send_mail_task(subject, text, email):
    send_mail(subject=subject,
              message=text,
              recipient_list=[email],
              from_email='admin@example.com')


@celery_app.task
def delete_older_log():
    for log in Logger.objects.all().filter(
            logged_at=datetime.now() - timedelta(days=7)
    ):
        log.delete()


@celery_app.task
def update_token():
    for user in TeacherUser.objects.all():
        try:
            user.auth_token.delete()
        except ObjectDoesNotExist:
            pass
        Token.objects.create(user=user)
