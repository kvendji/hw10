# from django.contrib.auth import get_user_model
# from django.contrib.auth.decorators import login_required
import csv

from core.forms import GroupCreateForm,\
    StudentForm,\
    ContactUSForm,\
    RegistrationForm
from core.models import Group, Student
from core.tasks import send_mail_task

from django.db.models.aggregates import Avg, Count, Max, Min
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, \
    UpdateView, FormView, View
from django.template import loader


class GroupView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        groups = Group.objects.all().select_related()
        info_about_group = groups.annotate(
            student_count=Count('students'),
            avg_age=Avg('students__age'),
            min_age=Min('students__age'),
            max_age=Max('students__age')
        )
        context = {
            'info_about_group': info_about_group,
            'mas': [1, 2, 3, 4, 5],
            'some_str': 'Lorem ipsum',
            'groups': groups
        }
        return context


class CreateGroupView(CreateView):
    template_name = 'create.html'
    form_class = GroupCreateForm
    success_url = reverse_lazy('core:home')


class StudentView(TemplateView):
    template_name = 'students_list.html'

    def get_context_data(self, **kwargs):
        students = Student.objects.all().select_related()
        context = {
            'students': students
        }
        return context


class UpdateStudentView(UpdateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    form_class = StudentForm
    model = Student


class CreateStudentView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    form_class = StudentForm


# class DeleteStudentView(DeleteView):
#     model = Student
#     success_url = reverse_lazy('core:home')
#
#     def get_success_url(self):
#         return reverse_lazy('core:home')
#

class SendMailView(FormView):
    template_name = 'send_mail.html'
    success_url = reverse_lazy('core:home')
    form_class = ContactUSForm

    def form_valid(self, form):
        send_mail_task.delay(form.cleaned_data['title'],
                             form.cleaned_data['message'],
                             form.cleaned_data['email_from'])
        return super(SendMailView, self).form_valid(form)


class RegistrationView(CreateView):
    template_name = 'registration.html'
    success_url = reverse_lazy('core:home')
    form_class = RegistrationForm


class TeacherAccountsView(TemplateView):
    template_name = 'user_accounts.html'


class DownloadStudentList(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="students_list.csv"'  # noqa

        writer = csv.writer(response)
        writer.writerow(['first name', 'last name', 'phone', 'age', 'group'])
        for student in Student.objects.all():
            writer.writerow([student.first_name,
                             student.last_name,
                             student.phone,
                             student.age,
                             student.group])
        return response


class DownloadGroupList(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="group_list.csv"'  # noqa
        for group in Group.objects.all():
            name_group = group.name
            teacher_group = group.teacher
        csv_data = (
            ('Name', 'Teacher'),
            (name_group, teacher_group)
        )

        student_file = loader.get_template('group_list.txt')
        context = ({
            'data': csv_data,
        })
        print(context)
        response.write(student_file.render(context))
        return response
