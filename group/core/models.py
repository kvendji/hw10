from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class TeacherUser(AbstractUser):

    class Meta:
        verbose_name = 'Препод'
        verbose_name_plural = 'Преподы'

    def __str__(self):
        return self.first_name


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    teacher = models.ForeignKey(get_user_model(),
                                on_delete=models.CASCADE,
                                verbose_name='Препод',
                                related_name="teacher_groups")

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

    def __str__(self):
        return f"{self.name} "


class Student(models.Model):
    first_name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255,
                                 verbose_name='Фамилия',
                                 null=True)
    phone = PhoneNumberField(null=False,)
    age = models.IntegerField(validators=[MinValueValidator(1),
                                          MaxValueValidator(100)],
                              verbose_name='Возраст')
    group = models.ForeignKey(Group,
                              on_delete=models.CASCADE,
                              verbose_name='Группа',
                              related_name='students')

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'

    def __str__(self):
        return f"{self.first_name}, {self.group}"


class Logger(models.Model):
    path = models.CharField(max_length=255, verbose_name='Путь')
    method = models.CharField(max_length=255, verbose_name='Метод')
    logged_at = models.DateTimeField(auto_now_add=True,
                                     editable=False,
                                     verbose_name='Дата Сохранения')

    class Meta:
        verbose_name = 'Регистратор'
        verbose_name_plural = 'Регистраторы'

    def __str__(self):
        return self.path
