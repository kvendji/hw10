from django import template

from core.models import TeacherUser

register = template.Library()


@register.filter(name='even_numb')
def get_even_numbers(mas):
    return [num for num in mas if num % 2 == 0]


@register.filter(name='count_words')
def get_count_words_in_string(some_str):
    return len(some_str.split(' '))


@register.inclusion_tag(filename='teacher_table.html')
def random_teacher(count):
    return {
        'teachers': TeacherUser.objects.all().order_by('?')[:count]
    }


@register.simple_tag(name='query_filter')
def query_filter(some_queryset, **kwargs):
    return some_queryset.filter(**kwargs)
