from core.models import Group, Student, TeacherUser
from django import forms


# class GroupCreateForm(forms.Form):
#     name = forms.CharField()
#     teacher = forms.ModelChoiceField(
#         queryset=TeacherModel.objects.all(),
#         widget=forms.widgets.RadioSelect()
#     )
#
#     def save(self):
#         track = GroupModel.objects.create(**self.cleaned_data)
#         return track


class GroupCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'teacher': forms.widgets.RadioSelect()
        }


# class StudentCreateForm(forms.Form):
#     name = forms.CharField()
#     age = forms.IntegerField(max_value=80,
#                              min_value=17
#                              )
#     group = forms.ModelChoiceField(queryset=Teacher.objects.all(),
#                                    widget=forms.widgets.CheckboxInput()
#                                    )


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'


class ContactUSForm(forms.Form):
    title = forms.CharField()
    message = forms.CharField()
    email_from = forms.EmailField()


class RegistrationForm(forms.ModelForm):
    password_confirmation = forms.CharField(max_length=255,
                                            widget=forms.PasswordInput)

    class Meta:
        model = TeacherUser
        fields = ('first_name',
                  'last_name',
                  'username',
                  'email',
                  'password',
                  'password_confirmation')

    def clean_password_confirmation(self):
        if self.cleaned_data['password'] !=\
                self.cleaned_data['password_confirmation']:
            raise forms.ValidationError('Пароли не совпадают')
        return self.cleaned_data['password']

    def save(self, commit=False):
        user = super(RegistrationForm, self).save(commit=True)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
