from django.urls import path
from django.urls import include
from rest_framework.routers import SimpleRouter

from api.views import TeacherViewSet, StudentViewSet, GroupViewSet

app_name = "api"

router = SimpleRouter()
router.register(r'teachers', TeacherViewSet)
router.register(r'students', StudentViewSet)
router.register(r'groups', GroupViewSet)


urlpatterns = [
    path('', include((router.urls, "api"), namespace='api')),
    path('teacher-list/',
         TeacherViewSet.as_view({'get': 'list'}),
         name='teacher-list'),
    path('students-list/',
         StudentViewSet.as_view({'get': 'list'}),
         name='student-list'),
    path('groups-list/',
         GroupViewSet.as_view({'get': 'list'}),
         name='group-list'),
]
