from django.contrib.auth.admin import UserAdmin

from core.models import Group, Student, TeacherUser, Logger

from django.contrib import admin
from django.utils.translation import gettext_lazy as _


class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone', 'age', 'group')
    search_fields = ('last_name',)
    list_filter = ('group', )


class LoggerAdmin(admin.ModelAdmin):
    fields = ('path', 'method', 'logged_at')
    readonly_fields = ('logged_at', )


class TeacherUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {
            'fields': ('is_active',
                       'is_staff',
                       'is_superuser',
                       'groups',
                       'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.register(TeacherUser, TeacherUserAdmin)
admin.site.register(Group)
admin.site.register(Student, StudentAdmin)
admin.site.register(Logger, LoggerAdmin)
